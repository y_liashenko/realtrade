$(document).ready(function() {

    // hide/show "build-stuff" block

    var buildStuff = $('.build-stuff');

    $('.full-stack').on('click', function() {
        if ( buildStuff.hasClass('hidden') ) {
            $(buildStuff).removeClass('hidden');
        } else {
            $(buildStuff).addClass('hidden');
        }
    });

// animation
    $(window).scroll(function(){
        var windowWidth = $(this).width();
        var windowHeight = $(this).height();
        var windowScrollTop = $(this).scrollTop();

        var addAnimation = function(){
                $('.trust-in li:even').find('img').addClass('fadeInLeft');
                $('.trust-in li:odd').find('img').addClass('fadeInRight');
            },
            removeAnimation = function() {
                $('.trust-in li:even').find('img').removeClass('fadeInLeft');
                $('.trust-in li:odd').find('img').removeClass('fadeInRight');
            },
            zoomInService = function() {
                $('.services').find('li').addClass('zoomIn');
                $('.services').find('li').css('opacity', '1');
            },
            zoomInadditional = function() {
                $('.additional').find('li').addClass('zoomIn');
                $('.additional').find('li').css('opacity', '1');
            };

            if(windowScrollTop>2100 && windowWidth>=1200 || windowScrollTop>3000 && windowWidth>=1000){
                addAnimation();
            } else {
                removeAnimation();
                $('.trust-in li').find('img').css('opacity', '1');
            }
            if(windowScrollTop>3700 && windowWidth<=2500 || windowScrollTop>6100 && windowWidth<=1200){
                zoomInService();
            }
            if(windowScrollTop>4600 && windowWidth<=2500 || windowScrollTop>7100 && windowWidth<=1200){
                zoomInadditional();
            }
    });

});